let express = require('express')
let app = express()
let server = require('http').createServer(app)
let io = require('socket.io').listen(server)

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/index.html')
})

io.sockets.on('connection', function (socket) {
  socket.emit('news', {
    hello: 'world'
  })
  socket.on('other event', function (data) {
    console.log(data);
  })
})

server.listen(8090)